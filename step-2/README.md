lambda-slack-example / step-2
=============================

In this directory you can find scripts helpful while reading
 [Lambda + Slack = health-check #2 — periodyczna funkcja]( http://timbercode.pl/blog/2017/04/03/lambda-slack-health-check-2/ )
 blog post (polish language).

AWS Lambda function
-------------------

You can find code of the function in
 file [simple-health-check.lambda.js]( simple-health-check.lambda.js ).
 Copy it's all content and paste on AWS Lambda as function body.
 
Remember to define environment variables:
 * `SLACK_WEBHOOK` - your Slack webhook URL
 * `SLACK_CHANNEL` - name of the Slack channel you want to post to
 * `HEALTH_CHECK_URL` - URL of the health-check of your system;
    preferably `GET` endpoint which responds with `200 OK` and 
    it's body has a JSON format
 * `SYSTEM_NAME` - "label" for you system to be used in messages
    post to Slack
    
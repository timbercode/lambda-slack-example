lambda-slack-example / step-3
=============================

In this directory you can find
[Node.js]( https://nodejs.org ) / [ claudia.js ]( https://claudiajs.com/ )
project described in
 [Lambda + Slack = health-check #3 — projekt z zależnościami]( http://timbercode.pl/blog/2017/04/18/lambda-slack-health-check-3/ )
 blog post (polish language).

setup
-----

1. Install Node.js. Preferably version 6.10 to match the version
   chosen for AWS Lambda runtime in further steps.

2. On [AWS IAM]( https://console.aws.amazon.com/iam/home ) create a new User (or configure existing one)
   with programmatic access and following policies:
    * `AWSLambdaFullAccess`
    * `AmazonAPIGatewayAdministrator`
    * `IAMFullAccess`
    
   Copy her "Access key ID" and "Secret access key" and put them in 
   `$HOME/.aws/credentials` on your development machine:
    ```
    [<your_profile_name>]
    aws_access_key_id = <your_access_key_id>
    aws_secret_access_key = <your_secret_access_key>
    ```
    where `<your_aws_profile_name>` is a name of this "profile", eg.
    `cli-user`
    
3. Install dependencies:
    ```bash
    npm install
    ```
    
4. Test that function works as expected:
    ```bash
    npm test
    ```

5. Configure AWS Lambda, then deploy current project as a new Lambda function:  
    ```bash 
    ./node_modules/.bin/claudia create \
        --profile <your_aws_profile_name>
        --region <your_chosen_aws_region> \
        --name <your_chosen_name_for_new_lambda> \
        --runtime nodejs6.10 \
        --handler src/lambda.handler \
        --version released \
        --set-env \
            NODE_ENV=production,SLACK_WEBHOOK='<slack_webhook>',SLACK_CHANNEL='<slack_channel>',HEALTH_CHECK_URL='<health_check_url>',SYSTEM_NAME='<system_name>'
    ```
    
    Environment variables above are:
     * `SLACK_WEBHOOK` - your Slack webhook URL
     * `SLACK_CHANNEL` - name of the Slack channel you want to post to
     * `HEALTH_CHECK_URL` - URL of the health-check of your system;
        preferably `GET` endpoint which responds with `200 OK` and 
        it's body has a JSON format
     * `SYSTEM_NAME` - "label" for you system to be used in messages
        post to Slack
        
    Of course you can omit lengthy `--set-env` part and define environment
    variables "by hand" on AWS website.
        
    On success, `claudia.json` is created. You probably want
    to add that file to repository because it contains
    data needed for further deployments of same Lambda
    function.
    
    If you make a mistake, you have to roll-back what you have done:
    * remove Lambda function (if created),
    * remove IAM Role (if created) - the one with name of your
      function and `-executor` suffix appended,
    * remove `claudia.json` file (if created).

deployment
----------

To deploy a newer version of the Lambda function just run:
```bash
env AWS_PROFILE=<your_aws_profile_name> npm run deploy
```

Afterwards you can try the function with fake event sent:
```bash
env AWS_PROFILE=<your_aws_profile_name> npm run try
```

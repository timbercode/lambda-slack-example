const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const externalSystem = require('../src/external-system');
const slackHealthCheck = require('../src/slack-health-check');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

const HEALTH_CHECK_URL = 'any.health.check.url';
const SLACK_WEBHOOK = 'any.slack.webhook';
const SLACK_CHANNEL = 'any-slack-channel';
const SYSTEM_NAME = 'Any System Name';

describe('SlackHealthCheck', function () {

    let HttpClient;

    beforeEach(function () {
        HttpClient = require('./httpClient.stub')();
    });

    describe('when system is healthy', function () {

        it('does not notify Slack', function (done) {
            // given
            HttpClient.onRequest('GET', HEALTH_CHECK_URL)
                      .respondsWith(200);
            const ExternalSystem = externalSystem({ HttpClient });
            const Slack = {
                sendMessageToChannel : sinon.stub().returns(Promise.resolve())
            };
            const SlackHealthCheck = slackHealthCheck({ ExternalSystem, Slack });

            // when
            const healthCheck = SlackHealthCheck.perform({
                healthCheckUrl : HEALTH_CHECK_URL
            });

            // then
            Slack.sendMessageToChannel.should.have.callCount(0);
            healthCheck.should.eventually.eql({
                result : 'system is healthy'
            }).notify(done)
        });

    });

    describe('when system is unhealthy', function () {

        [
            201, 204,
            400, 401, 404, 409,
            500, 502
        ].forEach(function (statusCodeOtherThan200) {
            it(`notifies Slack (status code ${statusCodeOtherThan200})`, function (done) {
                // given
                HttpClient.onRequest('GET', HEALTH_CHECK_URL)
                          .respondsWith(statusCodeOtherThan200, 'response body with details');
                const ExternalSystem = externalSystem({ HttpClient });
                const Slack = {
                    sendMessageToChannel : sinon.stub().returns(Promise.resolve())
                };
                const SlackHealthCheck = slackHealthCheck({ ExternalSystem, Slack });

                // when
                const healthCheck = SlackHealthCheck.perform({
                    healthCheckUrl : HEALTH_CHECK_URL,
                    slackWebhook   : SLACK_WEBHOOK,
                    slackChannel   : SLACK_CHANNEL,
                    systemName     : SYSTEM_NAME
                });

                // then
                healthCheck.should.eventually.eql({
                    result : 'system is NOT healthy'
                }).then(() => {
                    Slack.sendMessageToChannel.should.have.been.calledWith({
                        webhook : SLACK_WEBHOOK,
                        channel : SLACK_CHANNEL,
                        message : `:fire: ${SYSTEM_NAME} is *<${HEALTH_CHECK_URL}|unhealthy>* :fire:\n` +
                        `\`\`\`\n` +
                        `status : ${statusCodeOtherThan200}\n` +
                        `body   :\n` +
                        `response body with details` +
                        `\`\`\``
                    });
                }).should.notify(done);
            });
        });

        it('fails to notify Slack', function (done) {
            // given
            const statusCodeOtherThan200 = 500;
            HttpClient.onRequest('GET', HEALTH_CHECK_URL)
                      .respondsWith(statusCodeOtherThan200);
            const ExternalSystem = externalSystem({ HttpClient });
            const Slack = {
                sendMessageToChannel : sinon.stub().returns(Promise.reject('any Slack error'))
            };
            const SlackHealthCheck = slackHealthCheck({ ExternalSystem, Slack });

            // when
            const healthCheck = SlackHealthCheck.perform({
                healthCheckUrl : HEALTH_CHECK_URL
            });

            // then
            healthCheck.should.eventually.eql({
                error : 'any Slack error'
            }).notify(done);
        });

    });

    describe('when fails to check system health', function () {

        it('notifies Slack', function (done) {
            // given
            HttpClient.onRequest('GET', HEALTH_CHECK_URL)
                      .failsWith('unexpected error');
            const ExternalSystem = externalSystem({ HttpClient });
            const Slack = {
                sendMessageToChannel : sinon.stub().returns(Promise.resolve())
            };
            const SlackHealthCheck = slackHealthCheck({ ExternalSystem, Slack });

            // when
            const healthCheck = SlackHealthCheck.perform({
                healthCheckUrl : HEALTH_CHECK_URL,
                slackWebhook   : SLACK_WEBHOOK,
                slackChannel   : SLACK_CHANNEL,
                systemName     : SYSTEM_NAME
            });

            // then
            healthCheck.should.eventually.eql({
                error : 'unexpected error'
            }).then(() => {
                Slack.sendMessageToChannel.should.have.been.calledWith({
                    webhook : SLACK_WEBHOOK,
                    channel : SLACK_CHANNEL,
                    message : `:interrobang: failed to check health of ${SYSTEM_NAME} :interrobang:\n` +
                    `\`\`\`\n` +
                    `unexpected error` +
                    `\`\`\``
                });
            }).should.notify(done);
        });

        it('fails to notify Slack', function (done) {
            // given
            HttpClient.onRequest('GET', HEALTH_CHECK_URL)
                      .failsWith('unexpected error');
            const ExternalSystem = externalSystem({ HttpClient });
            const Slack = {
                sendMessageToChannel : sinon.stub().returns(Promise.reject('any Slack error'))
            };
            const SlackHealthCheck = slackHealthCheck({ ExternalSystem, Slack });

            // when
            const healthCheck = SlackHealthCheck.perform({
                healthCheckUrl : HEALTH_CHECK_URL
            });

            // then
            healthCheck.should.eventually.eql({
                error : 'any Slack error'
            }).notify(done);
        });

    });

});


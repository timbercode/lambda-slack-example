const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const slack = require('../src/slack');

chai.should();
chai.use(chaiAsPromised);

const SLACK_WEBHOOK = 'any.slack.webhook';
const SLACK_CHANNEL = 'any-slack-channel';
const SLACK_MESSAGE = 'Any Message';

describe('Slack', function () {

    let HttpClient;

    beforeEach(function () {
        HttpClient = require('./httpClient.stub')();
    });

    [200, 201, 204].forEach(function (statusCode2xx) {
        it('sends a message to Slack Webhook', function (done) {
            // given
            HttpClient.onRequest('POST', SLACK_WEBHOOK, {
                channel : SLACK_CHANNEL,
                text    : SLACK_MESSAGE
            }).respondsWith(statusCode2xx);
            const Slack = slack({ HttpClient });

            // when
            const notification = Slack.sendMessageToChannel({
                webhook : SLACK_WEBHOOK,
                channel : SLACK_CHANNEL,
                message : SLACK_MESSAGE
            });

            // then
            notification.should.be.fulfilled
                        .notify(done)
        });
    });

    [300, 400, 401, 403, 404, 500].forEach(function (statusCodeNot2xx) {
        it(`fails to send a message to Slack Webhook (status code: ${statusCodeNot2xx}`, function (done) {
            // given
            HttpClient.onRequest('POST', SLACK_WEBHOOK, {
                channel : SLACK_CHANNEL,
                text    : SLACK_MESSAGE
            }).respondsWith(statusCodeNot2xx);
            const Slack = slack({ HttpClient });

            // when
            const notification = Slack.sendMessageToChannel({
                webhook : SLACK_WEBHOOK,
                channel : SLACK_CHANNEL,
                message : SLACK_MESSAGE
            });

            // then
            notification.should.be.rejected
                        .notify(done)
        });
    });

});


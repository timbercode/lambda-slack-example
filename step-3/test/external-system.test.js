const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const externalSystem = require('../src/external-system');

chai.should();
chai.use(chaiAsPromised);

const HEALTH_CHECK_URL = 'any.health.check.url';

describe('ExternalSystem', function () {

    let HttpClient;

    beforeEach(function () {
        HttpClient = require('./httpClient.stub')();
    });

    it('does not format non-JSON response body', function (done) {
        // given
        HttpClient.onRequest('GET', HEALTH_CHECK_URL)
                  .respondsWith(200, 'not really { a: "JSON" }');
        const ExternalSystem = externalSystem({ HttpClient });

        // when
        const healthCheck = ExternalSystem.checkHealth({
            healthCheckUrl : HEALTH_CHECK_URL
        });

        // then
        healthCheck.should.eventually.eql({
            isHealthy      : true,
            responseStatus : 200,
            responseBody   : 'not really { a: "JSON" }'
        }).notify(done)
    });

    it('formats nicely JSON response body', function (done) {
        // given
        HttpClient.onRequest('GET', HEALTH_CHECK_URL)
                  .respondsWith(200, '{ "any": "thing", "sub": { "element1": 1, "element2": 2 }}');
        const ExternalSystem = externalSystem({ HttpClient });

        // when
        const healthCheck = ExternalSystem.checkHealth({
            healthCheckUrl : HEALTH_CHECK_URL
        });

        // then
        healthCheck.should.eventually.eql({
            isHealthy      : true,
            responseStatus : 200,
            responseBody   : '' +
            '{\n' +
            '  "any": "thing",\n' +
            '  "sub": {\n' +
            '    "element1": 1,\n' +
            '    "element2": 2\n' +
            '  }\n' +
            '}'
        }).notify(done)
    });

});


'use strict';

const sinon = require('sinon');

module.exports = httpClientStub;

function httpClientStub() {

    const requestStub = sinon.stub();

    return {
        request : requestStub,
        onRequest
    };

    function onRequest(method, url, jsonBody = null) {

        const callArguments = {
            method : method,
            url    : url
        };
        if (jsonBody) {
            callArguments.jsonBody = jsonBody;
        }

        return {
            respondsWith,
            failsWith
        };

        function respondsWith(statusCode, body = null) {
            requestStub
                .withArgs(callArguments)
                .returns(Promise.resolve({
                    status : statusCode,
                    body   : body ? body : JSON.stringify({ any : 'thing' })
                }));
        }

        function failsWith(error) {
            requestStub
                .withArgs({
                    method : method,
                    url    : url
                })
                .returns(Promise.reject(error));
        }

    }
}

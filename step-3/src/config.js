'use strict';

module.exports = {
    systemName     : process.env.SYSTEM_NAME,
    healthCheckUrl : process.env.HEALTH_CHECK_URL,
    slackWebhook   : process.env.SLACK_WEBHOOK,
    slackChannel   : process.env.SLACK_CHANNEL
};
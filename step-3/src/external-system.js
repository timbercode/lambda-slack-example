'use strict';

module.exports = externalSystem;

function externalSystem({ HttpClient }) {

    return {
        checkHealth
    };

    function checkHealth({ healthCheckUrl }) {
        return HttpClient
            .request({
                method : 'GET',
                url    : healthCheckUrl
            })
            .then(response => ({
                isHealthy      : response.status === 200,
                responseStatus : response.status,
                responseBody   : prettified(response.body)
            }))
            .catch(error => ({
                error : error
            }));
    }

    function prettified(text) {
        let asJson;
        try {
            asJson = JSON.parse(text);
        } catch (error) {
            return text;
        }
        const indentationSpaces = 2;
        return JSON.stringify(asJson, null, indentationSpaces);
    }

}
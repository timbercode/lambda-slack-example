'use strict';

module.exports = slackHealthCheck;

function slackHealthCheck({ ExternalSystem, Slack }) {

    return {
        perform
    };

    function perform({ healthCheckUrl, slackWebhook, slackChannel, systemName }) {
        return ExternalSystem
            .checkHealth({ healthCheckUrl })
            .then(({ error, isHealthy, responseStatus, responseBody }) => {
                if (error) {
                    return Slack.sendMessageToChannel({
                        webhook : slackWebhook,
                        channel : slackChannel,
                        message : slackErrorMessage(error)
                    }).then(() => ({
                        error : error
                    }));
                }
                if (!isHealthy) {
                    return Slack.sendMessageToChannel({
                        webhook : slackWebhook,
                        channel : slackChannel,
                        message : slackSystemUnhealthyMessage(responseStatus, responseBody)
                    }).then(() => ({
                        result : 'system is NOT healthy'
                    }));
                }
                return {
                    result : 'system is healthy'
                };
            })
            .catch(error => ({
                error : error
            }));

        function slackSystemUnhealthyMessage(status, body) {
            return `:fire: ${systemName} is *<${healthCheckUrl}|unhealthy>* :fire:\n` +
                `\`\`\`\n` +
                `status : ${status}\n` +
                `body   :\n` +
                `${body}` +
                `\`\`\``
        }

        function slackErrorMessage(error) {
            return `:interrobang: failed to check health of ${systemName} :interrobang:\n` +
                `\`\`\`\n` +
                `${error}` +
                `\`\`\``
        }
    }

}


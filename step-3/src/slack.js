'use strict';

module.exports = slack;

function slack({ HttpClient }) {

    return {
        sendMessageToChannel
    };

    function sendMessageToChannel({ webhook, channel, message }) {
        return HttpClient
            .request({
                method   : 'POST',
                url      : webhook,
                jsonBody : {
                    channel : channel,
                    text    : message
                }
            })
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    return Promise.reject(
                        `Slack notification request returned ${response.status} ` +
                        `instead of 2xx. Response body: ${response.body}`);
                }
            });
    }

}

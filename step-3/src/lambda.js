'use strict';

const {
    healthCheckUrl,
    slackWebhook,
    slackChannel,
    systemName
} = require('./config');
const HttpClient = require('./httpClient');
const ExternalSystem = require('./external-system')({ HttpClient });
const Slack = require('./slack')({ HttpClient });

const SlackHealthCheck = require('./slack-health-check')({ ExternalSystem, Slack });

exports.handler = (event, context, callback) => {
    SlackHealthCheck
        .perform({
            healthCheckUrl,
            slackWebhook,
            slackChannel,
            systemName
        })
        .then(({ result, error }) => {
            if (error) callback(error, null);
            else callback(null, result)
        });
};


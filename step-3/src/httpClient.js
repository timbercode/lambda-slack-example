'use strict';

const requestPromise = require('request-promise');

module.exports = {
    request
};

function request({
                     method = 'GET',
                     url,
                     jsonBody = null
                 }) {

    const requestParams = {
        method                  : method,
        uri                     : url,
        headers                 : {},
        simple                  : false,
        resolveWithFullResponse : true,
        timeout                 : 500
    };
    if (jsonBody) {
        requestParams.headers['content-type'] = 'application/json'
        requestParams.body = JSON.stringify(jsonBody)
    }

    return requestPromise(requestParams)
        .then(response => ({
            status : response.statusCode,
            body   : response.body
        }));

}

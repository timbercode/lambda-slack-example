lambda-slack-example
====================

Code related to
[blog posts series on timbercode.pl tagged `lambda-slack`.](https://timbercode.pl/blog/tag/lambda-slack)
 
Structure
-----------

* [step-1]( step-1/ ) – scripts helpful while reading
  [Lambda + Slack = health-check #1 — konfiguracja]( https://timbercode.pl/blog/2017/03/25/lambda-slack-health-check-1/ )
* [step-2]( step-2/ ) – scripts helpful while reading
  [Lambda + Slack = health-check #2 — periodyczna funkcja]( https://timbercode.pl/blog/2017/04/03/lambda-slack-health-check-2/ )
* [step-3]( step-3/ ) – scripts helpful while reading
  [Lambda + Slack = health-check #3 — projekt z zależnościami]( https://timbercode.pl/blog/2017/04/18/lambda-slack-health-check-3/ )
 
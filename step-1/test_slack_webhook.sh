#!/usr/bin/env bash

slack_webhook=$1

if [ -z "${slack_webhook}" ]; then
    echo "You have to provide Slack webhook URL as the first parameter"
    exit 1
fi

curl \
      -X POST \
      -H 'Content-Type: application/json' \
      --data '{ "text": "Hello!" }' \
      ${slack_webhook}
lambda-slack-example / step-1
=============================

In this directory you can find scripts helpful while reading
 [Lambda + Slack = health-check #1 — konfiguracja]( http://timbercode.pl/blog/2017/03/25/lambda-slack-health-check-1/ )
 blog post (polish language).

Slack webhook
-------------
 
To test your Slack webhook run:
```
$ ./test_slack_webhook.sh <your_slack_webhook_url_here>
```

AWS Lambda function
-------------------

You can find code of the function in
 file [hello-to-slack.lambda.js]( hello-to-slack.lambda.js ).
 Copy it's all content and paste on AWS Lambda as function body.
 Remember to set your Slack webhook URL as `SLACK_WEBHOOK`
 environment variable.